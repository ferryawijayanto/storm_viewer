//
//  ContentView.swift
//  StormViewer
//
//  Created by Ferry Adi Wijayanto on 17/11/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
